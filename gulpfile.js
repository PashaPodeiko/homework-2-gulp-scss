const gulp = require("gulp");
const htmlmin = require("gulp-htmlmin");
const concat = require("gulp-concat");
const autoprefixer = require("gulp-autoprefixer");
const cssmin = require("gulp-cssmin");
const rename = require("gulp-rename");
const minify = require("gulp-minify");
const deleteFiles = require("delete");
const sass = require("gulp-sass")(require("sass"));
const purgecss = require('gulp-purgecss');
const browserSync = require('browser-sync').create();

gulp.task("html-minify", function () {
	return gulp
		.src("src/html/*.html")
		.pipe(htmlmin({ collapseWhitespace: true }))
		.pipe(gulp.dest("dist"));
});

gulp.task("buildImg", function () {
	return gulp.src("src/img/**/*").pipe(gulp.dest("dist/img"));
});

gulp.task("buildJs", function () {
	return gulp.src("src/js/*.js")
		.pipe(minify())
		.pipe(rename({ suffix: ".min" }))
		.pipe(gulp.dest("dist"));
});

gulp.task("deleteFiles", function () {
	return deleteFiles("build/");
});

gulp.task("build-css", function () {
	return (
		gulp
			.src("src/scss/style.scss")
			.pipe(sass().on("error", sass.logError))
			.pipe(
				autoprefixer({
					cascade: false,
				})
			)
			.pipe(cssmin())
			.pipe(purgecss({
				content: ['src/**/*.html']
			}))
			.pipe(rename({ suffix: ".min" }))
			.pipe(browserSync.stream())
			.pipe(gulp.dest("dist"))
	);
});

gulp.task('dev', function () {
	browserSync.init({
		server: {
			baseDir: 'dist'
		}
	});
	gulp.watch(["src/scss/**/**/*.scss", "src/html/*.html", "src/js/*.js"], gulp.parallel('build'));
	gulp.watch(["dist/*.html", "dist/*css"]).on("change", browserSync.reload)
});

gulp.task(
	"build",
	gulp.series(
		"deleteFiles",
		gulp.parallel("html-minify", "buildJs", "build-css", "buildImg")
	)
);

gulp.watch(
	["src/scss/**/**/*.scss", "src/html/*.html", "src/js/*.js"],
	gulp.series("build")
);